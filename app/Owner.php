<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $table = 'owner';

    public function cars()
    {
        return $this->hasMany('App\Car');
    }
}
