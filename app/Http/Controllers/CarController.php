<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
class CarController extends Controller
{
    public function edit($id, Request $request)
    {
        return view('car_edit', ['car' => Car::find($id)]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'reg_number' => 'required|max:6',
            'model' => 'required|max:32',
            'brand' => 'required|max:32',
        ]);
    }
}
