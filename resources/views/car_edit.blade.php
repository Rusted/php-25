@extends('layout')

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('car_update', ['id' => $car->id]) }}" method="POST">
<div class="form-group @if ($errors->has('reg_number')) has-error @endif">
        <label class="col-md-2 control-label">Reg. numeris</label>
        <div class="col-md-10">
            @if ($errors->has('reg_number'))
            <small class="text-danger">
                {{$errors->first('reg_number')}}
            </small>
            @endif
            <input type="text" name="reg_number" class="form-control" placeholder="Reg. numeris" value="{{ Request::old('reg_number') ?  Request::old('reg_number'): $car->reg_number }}">
        </div>
    </div>
    Modelis:<input type="text" name="model">
    Gamintojas:<input type="text" name="brand">
    {{csrf_field()}}
    <input type="submit">
</form>
@endsection